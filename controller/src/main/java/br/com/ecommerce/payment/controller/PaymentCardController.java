package br.com.ecommerce.payment.controller;

import br.com.ecommerce.payment.mapper.PaymentCardContractRequestMapper;
import br.com.ecommerce.payment.model.PaymentCardContractRequest;
import br.com.ecommerce.payment.service.PaymentCardService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@AllArgsConstructor
@RestController
@RequestMapping(path = "/v1/payment-card")
public class PaymentCardController {

    private final PaymentCardService paymentCardService;

    @PostMapping
    public Mono<Void> addCard(@RequestBody PaymentCardContractRequest paymentCardContractRequest) {
        final var paymentCard = PaymentCardContractRequestMapper.getMapper().mapper(paymentCardContractRequest);

        return paymentCardService.addCard(paymentCard).then();
    }
}
