package br.com.ecommerce.payment.test;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@AllArgsConstructor
@RestController
@RequestMapping("/test")
public class TestConnectionController {

    @GetMapping()
    public String testConnection() {
        return "Test connection";
    }
}
