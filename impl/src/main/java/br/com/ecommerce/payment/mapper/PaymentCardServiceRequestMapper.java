package br.com.ecommerce.payment.mapper;

import br.com.ecommerce.payment.model.PaymentCard;
import br.com.ecommerce.payment.model.PaymentCardServiceRequest;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface PaymentCardServiceRequestMapper {

    static PaymentCardServiceRequestMapper getMappers() {
        return Mappers.getMapper(PaymentCardServiceRequestMapper.class);
    }

    PaymentCard mapper(PaymentCardServiceRequest paymentCardServiceRequest);
}
