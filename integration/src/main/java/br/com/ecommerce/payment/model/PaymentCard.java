package br.com.ecommerce.payment.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@Document(collection = "payment-card")
public class PaymentCard {
    @Id
    private String id;
    private Long cardNumber;
    private String expiryDate;
    private Integer cvv;
    private String holderName;
}
