package br.com.ecommerce.payment.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class PaymentCardContractRequest {
    private Long cardNumber;
    private String expiryDate;
    private Integer cvv;
    private String holderName;
}
