package br.com.ecommerce.payment.service;

import br.com.ecommerce.payment.integration.PaymentCardIntegration;
import br.com.ecommerce.payment.mapper.PaymentCardServiceRequestMapper;
import br.com.ecommerce.payment.model.PaymentCardServiceRequest;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
@AllArgsConstructor
public class PaymentCardService {

    private final PaymentCardIntegration paymentCardIntegration;

    public Mono<Void> addCard(PaymentCardServiceRequest paymentCardServiceRequest) {
        final var paymentCard = PaymentCardServiceRequestMapper.getMappers().mapper(paymentCardServiceRequest);

        return paymentCardIntegration.save(paymentCard).then();
    }
}
