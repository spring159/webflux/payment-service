package br.com.ecommerce.payment.integration;

import br.com.ecommerce.payment.model.PaymentCard;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PaymentCardIntegration extends ReactiveMongoRepository<PaymentCard, String> {
}
