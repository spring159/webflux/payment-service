package br.com.ecommerce.payment.mapper;

import br.com.ecommerce.payment.model.PaymentCardContractRequest;
import br.com.ecommerce.payment.model.PaymentCardServiceRequest;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface PaymentCardContractRequestMapper {

    static PaymentCardContractRequestMapper getMapper() {
        return Mappers.getMapper(PaymentCardContractRequestMapper.class);
    }

    PaymentCardServiceRequest mapper(PaymentCardContractRequest paymentCardContractRequest);
}
